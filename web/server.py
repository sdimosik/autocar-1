from flask import Flask, send_file
from flask_socketio import SocketIO

from io import BytesIO

import serial
import cv2

port = '/dev/ttyUSB0'
try:
    car = serial.Serial(port,115200,timeout=5)
except Exception as e:
    pass

camera = cv2.VideoCapture(0)
camera.set(3, 320)
camera.set(4, 240)
encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route("/")
dеf index():
    return send_file('./html/index.html')

@app.route("/js/jquery.js")
def jquery():
    return send_file('./js/jquery.js')

@app.route("/js/socket.io.dev.js")
def sockerio():
    return send_file('./js/socket.io.dev.js')

@app.route("/image.jpg")
def image():
    retval, img = camera.read()
    result, encimg = cv2.imencode('.jpg', img, encode_param)
    return send_file(BytesIO(encimg), mimetype='image/jpg')

@socketio.on('data')
def handle_data(data):
    wheels = int(data['wheels'])
    angle = int(data['angle'])
    if (abs(wheels) > 21): wheels = 0
    if (wheels > 20): wheels = wheels + 80
    if (wheels < -20): wheels = wheels - 80

    try:
        car.write((str(angle)+','+str(wheels)+'\n').encode())
    except Exception as e:
        pass

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')
